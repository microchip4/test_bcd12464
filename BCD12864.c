/*************************************************
  Copyright (C), 2010, MF_Zou / (C) Copyright 2010
  All Rights Reserved
  Source File Name: BCD12864.c
  Date:             2011.09.03
  Version:          V2011 Build 0903
  Author:           MF Zou
  Description:      MSP430G2xx Example
                    -> BCD12864
  Others:
  Function List:
  Revision History:
    1. Date:
       Author:
       Modification:
    2. ...
*************************************************/

#include "main.h"

void LCD_PortConfig()
{   P1SEL = 0x00;
    P2SEL = 0x00;
    P1OUT |= (BIT7 | BIT6 | BIT5 | BIT4);
    P1REN |= (BIT7 | BIT6 | BIT5 | BIT4);
    P1DIR |= (BIT7 | BIT6 | BIT5 | BIT4);
    P2OUT |= BIT7;			//P2.7 pullup
    P2REN |= BIT7;			//P2.7 pullup
    P2DIR |= BIT7;//SDA
    
    P2OUT |= BIT6;
    P2DIR &= ~BIT6;   //P2.6 set as input for BCD_BUSY
    P2REN |= BIT6;
}
//**********************************************
//---- Data input function ----
void input_data( unsigned char Data)
{ unsigned char temp;
  unsigned char i;
  P1OUT |= BIT4;  //DC=1
  P1OUT &= ~BIT5; //CS=0
  temp = Data;
  while (BIT6 & P2IN)   {; };              //check the busy status of IC
  for (i=0; i<8; i++)
  {
      if ((temp&0x80)==0x80)        P2OUT |= BIT7;  //SDA=1
      else        P2OUT &= ~BIT7; //SDA=0
      Delay_nus(5);
      P1OUT &= ~BIT7; //SCLK=0
      Delay_nus(5);
      P1OUT |= BIT7;  //SCLK=1
      Delay_nus(5);
//      P1OUT &= ~BIT7; //SCLK=0
//      Delay_nus(5);
      temp = temp<<1;         //shift one bit.
  } 
//      P1OUT &= ~BIT7;//SCLK=0
  P1OUT |= BIT5;  //CS=1
}

//--- Command input function ----
void input_command(unsigned char command)
{ unsigned char temp;
  unsigned char i;

  P1OUT &= ~BIT4; //DC=0
  P1OUT &= ~BIT5; //CS=0
  temp = command;
  while (BIT6 & P2IN)   {; };              //check the busy status of IC
  for (i=0; i<8; i++)
  {
      if ((temp&0x80)==0x80)      P2OUT |= BIT7;  //SDA=1
      else        P2OUT &= ~BIT7; //SDA=0
//      Delay_nus(5);
//      P1OUT |= BIT7;  //SCLK=1
      Delay_nus(5);
      P1OUT &= ~BIT7; //SCLK=0
      Delay_nus(5);
      P1OUT |= BIT7;  //SCLK=1
      Delay_nus(5);
      temp = temp<<1;         //shift one bit.
  } 
//      P1OUT &= ~BIT7;
  P1OUT |= BIT5;  //CS=1
}

//---------------------------- Clear and Drive repeat times function ------------//
void Repeat_set (unsigned char repeat_times1, unsigned char repeat_times2, unsigned char repeat_times3)
{
      input_command (0x93);                   //VA clear repeat times
      input_command (repeat_times1);

      input_command (0x94);                   //VA idle repeat times
      input_command (repeat_times1);

      input_command (0x95);                   //AA clear repeat times
      input_command (repeat_times2);

      input_command (0x96);                   //AA idle repeat times
      input_command (repeat_times2);

      input_command (0x97);                   //Driving repeat times 
      input_command (repeat_times3);            
}

//---------------------------- Reset the Drive IC function ---------------------//
void BCD12864_Reset(void)
{   //P1OUT &= ~BIT7;//SCLK=0
    P1OUT |= BIT7;//SCLK=1
    //DevicePort->nRES = 0;
    P1OUT &= ~BIT6;//Res
    Delay_nms(100);
    //DevicePort->nRES = 1;
    P1OUT |= BIT6;
    Delay_nms(50);
}
//------------------------------- Clearing method setting function ---------------//
void Clear_disp (void)
{
        Repeat_set (1,1,0);
        input_command (0x32);
        input_command (0x21);
        input_command (0x31);     //Scheme B clearing

        Repeat_set (0,0,1);
        input_command (0x32);
        input_command (0x20);    //Scheme A Driving
}

void Disp_Logo(unsigned char sX, unsigned char sY)
{
        unsigned char page ; 
        unsigned char disp_segL, disp_segH;       
	unsigned char disp_seg;
	unsigned long int pointer;
	pointer=0;
        for (disp_seg=sX;disp_seg<(32+sX);disp_seg++)             //disp_seg=128.
        {   disp_segL=disp_seg&0x0f;
            disp_segH=(disp_seg>>4);
	    for (page=(0xb0+sY);page<(0xb4+sY);page++)
            {        
		input_command(disp_segL);             //set the segment start address
    	    	input_command(disp_segH | 0x10);
            	input_command(page); 
            	input_data(Logo3232[pointer++]);
	    }
        }
//        Clear_disp ();                  //Clearing the display before Driving an image.
//        input_command(0x31);            //Driving update command
}


//----------------------------- Enter the sleep method ------------------//
void Enter_sleep (void)
{
        input_command (0xe9);
        input_command (0x00);              //Disable the Bias Driven

        input_command (0xa3);    
        input_command (0x00);             //Disable the Analog Circuitry
 
        input_command (0xf6);
        input_command (0x00);             //Disable the OSC
  
        input_command (0x2e);             //Disable the Voltage Buffer.

        input_command(0x31); 
}
/*
//------------------------------ Long clearing function. ---------------//
//:Input Variable: "TclrVal"------>'0x1C': it will delay 1S.
//:				   '0x1D': it will delay 2S.	
//:				   '0x1E': it will delay 4S.
//:				   '0x1F': it will delay 10S.		
void LongClr(unsigned int TclrVal)
{
	Repeat_set (1,1,0);

	input_command (0x80); 		// 3) set driving parameters
       	input_command (0x00); 		//
     	input_command (TclrVal); 	// 4) set VA clear hold time 
        input_command (0x0A); 		//--->Idle hold time.
        input_command (0x15); 		// 5) set VA idle hold time, AA clear hold time
        input_command (0x0A); 		//--->Idle hold time.
        input_command (0x0C); 		// 6) set AA idle hold time, Driving using time
        input_command (0x40); 		//
        input_command (0x40); 		// 7) set clear and drive voltage (Vlcd = 24V)
	input_command (0x40);

	input_command (0x31);
}
*/

void BCD12864_SetXY(unsigned char X, unsigned char Y) 
{ unsigned char X_L, X_H;
  X_L = X & 0x0f;
  X_H = X >> 4;
	        
  input_command(X_L);
  input_command(0x10 + X_H);
  input_command(0xB0 + Y);//page
}

void BCD12864_WriteChar(unsigned char c) 
{
    unsigned char Line;
    unsigned char Index = c - 32;
    for (Line = 0; Line < 6; Line++)
        input_data(font6x8[Index][Line]);
}

void BCD12864_WriteString(unsigned char* s)
{
  while(*s){ BCD12864_WriteChar(*s++);};
}

void Clr_Ram()
{ unsigned char i, seg;
  for(i=0;i<8;i++)
  {
      BCD12864_SetXY(0x00,i);
      for (seg = 0; seg < 128; seg++)
      { input_data(0x00);}
  }
}

//----------------------------- Initialize the Drive IC function ----------------//
void BCD12864_Init (unsigned char disp_mode)
{
       input_command (0xA3);
       input_command (0x1A); // 1) enable analog block

       input_command (0xF6);
       input_command (0x40); // 2) enable oscillator

       input_command (0x80); // 3) set driving parameters
       input_command (0x00); //
       input_command (0x0C); // 4) set VA clear hold time 
       input_command (0x0A); //
       input_command (0x15); // 5) set VA idle hold time, AA clear hold time
       input_command (0x0A); //
       input_command (0x0C); // 6) set AA idle hold time, Driving using time
       input_command (0x40); //
       input_command (0x40); // 7) set clear and drive voltage (Vlcd = 24V)

       input_command (0xE9); //
       input_command (0x84); // 8) enable bias ladder 

       input_command (0x2F); // 9) enable booster

       input_command (0xA8); //
       input_command (0x40); // 10) set multiplex ratio 64

       input_command (0xA2); //
       input_command (0x02); // 11) set module bias 1/7

//       input_command (0xA6); // 12) set normal (non-reversed) display
       input_command (disp_mode); // 12) set reversed display

       input_command (0xA0); // 13) set segment remap (0-->127)

       input_command (0xC8); // 14) set common remap (C63-->C0)

       input_command (0x40); // 15) set display start line (C0)

       Repeat_set (0,0,0);   // set the Clearing and Driving repeat times.

       input_command (0x31); // Driving update command
        
       Clr_Ram();
}

void Reflash_disp()
{
   Clear_disp ();                  //Clearing the display before Driving an image.
   input_command(0x31);            //Driving update command

}